package com.epam.test.automation.java.practice4;

public class Task4 {
    private Task4(){}
    public static double sumGeometricElements(int a1, double t, int alim) {
        if(a1<=alim||t>=1||t<=0||a1<0 &&alim>0 ||a1>0 && alim<0){
            throw new IllegalArgumentException();
        }
        double temp = a1;
        double sum = 0;
        int i =0;
        while (temp > alim&& i<100)
        {
            sum += temp;
            temp*=t;
            i++;
        }
        return Math.round(sum);
    }
}
