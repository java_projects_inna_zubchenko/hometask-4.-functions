package com.epam.test.automation.java.practice4;

public class Task1 {

    private Task1(){}
    public static boolean isSorted(int[] array, SortOrder order)
    {
        if(array == null||array.length == 0)
        {throw new IllegalArgumentException();}
        for (int i = 1; i < array.length; i++)
        {
            if ((array[i] < array[i - 1] && order == SortOrder.ASC) || (array[i] > array[i - 1] && order == SortOrder.DESC))
                return false;
        }
        return true;
    }
}
