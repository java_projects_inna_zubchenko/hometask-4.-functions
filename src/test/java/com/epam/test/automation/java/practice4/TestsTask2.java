package com.epam.test.automation.java.practice4;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestsTask2 {
    @DataProvider(name = "testTransform")
    public Object[][] dataProviderMethod2(){
        return new Object[][] { {new int[]{12, 9, 7, 3, 1}, SortOrder.DESC, new int[]{12, 10, 9, 6, 5}},
                {new int[]{1, 3, 7, 15, 22}, SortOrder.ASC, new int[]{1, 4, 9, 18, 26}},
                {new int[]{1, 3, 7, 5}, SortOrder.ASC, new int[]{1, 3, 7, 5}}};
    }

    @Test(dataProvider = "testTransform")
    public void transformTest(int[] array, SortOrder order, int[] expected){
        var actual = Task2.transform(array, order);
        Assert.assertEquals(actual, expected, "The method isSorted returns incorrect value");
    }
    @Test()
    public void exceptionTask2Test() {
        try {
            Task2.transform(null, SortOrder.DESC);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException expected) {}
    }
}
