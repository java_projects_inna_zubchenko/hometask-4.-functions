package com.epam.test.automation.java.practice4;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestsTask4 {
    @DataProvider(name = "testSumGeometricElements")
    public Object[][] dataProviderMethod4(){
        return new Object[][] {{100, 0.5, 0, 200},{100, 0.5, 20, 175},{100, 0.9, 50, 522},{5, 0.9, 0, 50}};
    }

    @Test(dataProvider = "testSumGeometricElements")
    public void sumGeometricElementsTest(int a1, double t, int alim, double expected){
        var actual = Task4.sumGeometricElements(a1, t, alim);
        Assert.assertEquals(actual, expected, "The method multiArithmeticElements returns incorrect value");
    }
    @Test()
    public void exceptionTask4Test() {
        try {
            Task4.sumGeometricElements(3, 2, 1);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException expected) {}
    }
}

