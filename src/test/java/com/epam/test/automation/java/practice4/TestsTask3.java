package com.epam.test.automation.java.practice4;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public final class TestsTask3 {
    @DataProvider(name = "testMultiArithmeticElements")
    public Object[][] dataProviderMethod3(){
        return new Object[][] { {1, 2, 5, 945}, {3, 3, 3, 162}, {12, 1, 2, 156}};
        }

    @Test(dataProvider = "testMultiArithmeticElements")
    public void multiArithmeticElementsTest(int a1, int t, int n, int expected){
        var actual = Task3.multiArithmeticElements(a1, t, n);
        Assert.assertEquals(actual, expected, "The method multiArithmeticElements returns incorrect value");
    }
    @Test()
    public void exceptionTask3Test() {
        try {
            Task3.multiArithmeticElements(1, 3, -5);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException expected) {}
    }
}
