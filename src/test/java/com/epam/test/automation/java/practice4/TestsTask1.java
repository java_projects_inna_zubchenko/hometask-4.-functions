package com.epam.test.automation.java.practice4;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestsTask1 {
    @DataProvider(name = "testIsSorted")
    public Object[][] dataProviderMethod1(){
        return new Object[][] { {new int[]{12, 9, 7, 3, 1}, SortOrder.DESC, true},
                {new int[]{1, 3, 7, 15, 22}, SortOrder.ASC, true},
                {new int[]{1, 3, 7, 5}, SortOrder.ASC, false}};
    }

    @Test(dataProvider = "testIsSorted")
    public void isSortedTest(int[] array, SortOrder order, boolean expected){
        var actual = Task1.isSorted(array, order);
        Assert.assertEquals(actual, expected, "The method isSorted returns incorrect value");
    }
    @Test()
    public void exceptionTask1Test() {
        try {
            Task1.isSorted(null, SortOrder.DESC);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException expected) {}
    }
}
